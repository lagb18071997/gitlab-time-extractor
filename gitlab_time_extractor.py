#!/usr/bin/env python3

import sys
import os
import argparse
import requests

# Get User of environment Variable
default_uri = os.environ.get("GITLAB_URL")
default_token = os.environ.get("GITLAB_TOKEN")
print("")
print("Environment Variables: ")
print("GITLAB_URL = " + str(default_uri))
print("GITLAB_TOKEN = " + str(default_token))
print("")

# Set Arguments for CLI
parser = argparse.ArgumentParser(
    description="Show Tracking of Issues for startDate to endDate",
    epilog="""
    		    ***Necessary Variables: 
    		    	Set GITLAB_USER, GITLAB_URL and GITLAB_TOKEN as Environment-Variables***
    		    	""")
parser.add_argument("--startDate",
                    type=str,
                    help="StartDate of Tracking (YYYY-MM-DD)",
                    required=True)
parser.add_argument("--endDate",
                    type=str,
                    help="EndDate of Tracking (YYYY-MM-DD)",
                    required=True)
parser.add_argument(
    "--user",
    type=str,
    help="Filter by User",
)
parser.add_argument("--projectId", type=str, help="Filter by Project")
args = parser.parse_args()


def run_query(uri, gitlab_token, user, projectId, start_date, end_date):
    if uri is None or gitlab_token is None:
        print("Error: GITLAB_TOKEN is None or GITLAB_URL is None")
        print("Please set them as environment variables!")
        print("""
usage: gitlab_time_extractor.py [-h] [--startDate STARTDATE] [--endDate ENDDATE] [--user USER] [--projectId PROJECTID]

Show Tracking of Issues for startDate to endDate

options:
  -h, --help            show this help message and exit
  --startDate STARTDATE
                        StartDate of Tracking (YYYY-MM-DD)
  --endDate ENDDATE     EndDate of Tracking (YYYY-MM-DD)
  --user USER           Filter by User
  --projectId PROJECTID Filter by Project

***Necessary Variables: Set GITLAB_USER, GITLAB_URL and GITLAB_TOKEN as Environment-Variables**
        """)
        sys.exit(0)
    # Query-Parameter setzen
    if user is None:
        user_query = ""
        print("All Users: ")
    else:
        print("User: " + user)
        user_query = f'username: "{user}"'

    if projectId is None:
        project_query = ""
        print("All Projects: ")
    else:
        print("ProjectId: " + projectId)
        project_query = "" if user is None else ", "
        project_query += f'projectId: "gid://gitlab/Project/{projectId}"'

    if start_date is None:
        start_date_query = ""
        print("StartDate: 0")
    else:
        start_date_query = "" if (user is None and projectId is None) else ", "
        print("StartDate: " + start_date)
        start_date_query += f'startDate: "{start_date}"'

    if end_date is None:
        end_date_query = ""
        print("EndDate: infinite")
    else:
        end_date_query = "" if (user is None and projectId is None and
                                start_date is None) else ", "
        print("EndDate: " + end_date)
        end_date_query += f'endDate: "{end_date}"'

    # Query-Abfrage setzen
    query = f"""query {{
  timelogs({user_query}{project_query}{start_date_query}{end_date_query}) {{
    nodes {{
      issue {{
        title
        labels {{
          nodes {{
            title
          }}
        }}
      }}
      timeSpent
      
      spentAt
    }}
  }}
}}"""
    status_code = 200
    headers = {"Authorization": "Bearer " + str(gitlab_token)}
    uri += "/api/graphql"

    request = requests.post(str(uri), json={'query': query}, headers=headers)
    if request.status_code == status_code:
        return request.json()
    else:
        raise Exception(
            f"Unexpected status code returned: {request.status_code}")


def edit_query(r):
    data = r['data']['timelogs']['nodes']
    # Reihenfolge invertieren
    data = data[::-1]
    if len(data) == 0:
        print("Query: Empty")
    # Sekunden in Stunden umwandeln
    for object in data:
        object["timeSpent"] = round(object["timeSpent"] / 3600, 2)

    # Datum auf YYYY-MM-DD kürzen
    for object in data:
        object['spentAt'] = object['spentAt'][:10]

    # Issues mit gleichem title, label & spentAt zusammenführen
    i = 0
    length_data = len(data)
    while i < length_data - 1:
        same_spent_at = data[i]["spentAt"] == data[i + 1]["spentAt"]
        same_issue = data[i]["issue"] == data[i + 1]["issue"]
        if same_spent_at and same_issue:
            data[i + 1]["timeSpent"] += data[i]["timeSpent"]
            del data[i]
            length_data -= 1
            i -= 1
        i += 1

    # Labels nach "account::" filtern, ansonsten "None" hineinschreiben
    for object in data:
        if object["issue"]["labels"]["nodes"] == []:
            object["issue"]["labels"]["nodes"].append({"title": "None"})
        else:
            account_found = False
            for label in object["issue"]["labels"]["nodes"]:
                if "account::" in label["title"]:
                    account_found = True
                    object["issue"]["labels"]["nodes"] = [{
                        "title": label["title"][9:]
                    }]
                    break
            if account_found is False:
                object["issue"]["labels"]["nodes"] = [{"title": "None"}]

    # timeSpentPerDay-Spalte hinzufügen
    start_index = 0
    for i in range(len(data) - 1):
        if data[i]["spentAt"] != data[i + 1]["spentAt"]:
            time_spent_per_day = 0
            for j in range(start_index, i + 1):
                time_spent_per_day += data[j]["timeSpent"]
            for j in range(start_index, i + 1):
                data[j]["timeSpentPerDay"] = time_spent_per_day
            start_index = i + 1
        if i == len(data) - 2:
            time_spent_per_day = 0
            for j in range(start_index, i + 2):
                time_spent_per_day += data[j]["timeSpent"]
            for j in range(start_index, i + 2):
                data[j]["timeSpentPerDay"] = time_spent_per_day
    return data


def print_data(data):
    # Erstes Element als Caption einfügen
    data.insert(
        0, {
            "issue": {
                "title": "Issues",
                "labels": {
                    "nodes": [{
                        "title": "Account"
                    }]
                }
            },
            "timeSpent": "timeSpent",
            "spentAt": "spentAt",
            "timeSpentPerDay": "timeSpentPerDay"
        })
    # Spaltenbreiten ermitteln
    max_length_title = max([len(object["issue"]["title"]) for object in data])

    max_length_label = max([
        len(object["issue"]["labels"]["nodes"][0]["title"]) for object in data
    ])

    max_length_spent_at = max([len(object["spentAt"]) for object in data])

    max_length_time_spent = max(
        [len(str(object["timeSpent"])) for object in data])

    max_length_time_spent_per_day = max(
        [len(str(object["timeSpentPerDay"])) for object in data])

    table_width = max_length_spent_at + max_length_title + max_length_label + max_length_time_spent + max_length_time_spent_per_day + 4

    # Tabellenausgabe
    print("(timeSpent in hours)")
    print("\n")
    line = data[0]["spentAt"].ljust(max_length_spent_at + 1)
    line += data[0]["issue"]["title"].ljust(max_length_title + 1)
    line += data[0]["issue"]["labels"]["nodes"][0]["title"].ljust(
        max_length_label + 1)
    line += str(data[0]["timeSpent"]).ljust(max_length_time_spent + 1)
    line += str(data[0]["timeSpentPerDay"])
    print(line)

    for i in range(1, len(data)):
        if data[i]["spentAt"] != data[i - 1]["spentAt"]:
            print("-" * table_width)
            line = data[i]["spentAt"] + " "
        else:
            line = " ".ljust(max_length_spent_at + 1)
        line += data[i]["issue"]["title"].ljust(max_length_title + 1)
        line += data[i]["issue"]["labels"]["nodes"][0]["title"].ljust(
            max_length_label + 1)
        line += str(data[i]["timeSpent"]).ljust(max_length_time_spent + 1)
        if data[i]["spentAt"] != data[i - 1]["spentAt"]:
            line += str(data[i]["timeSpentPerDay"])
        print(line)


# Ausführen der Methoden

result = run_query(default_uri, default_token, args.user, args.projectId,
                   args.startDate, args.endDate)

data = edit_query(result)

print_data(data)
